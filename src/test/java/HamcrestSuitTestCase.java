/**
 * Created by romelldominguez on 3/29/17.
 */
import Hamcrest.Todo;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Every.everyItem;
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class HamcrestSuitTestCase {
    @Test
    public void Assets(){
        List<Integer> list = Arrays.asList(5, 2, 4);

        assertThat(list, hasSize(3));

        // ensure the order is correct
        assertThat(list, contains(5, 2, 4));

        assertThat(list, containsInAnyOrder(2, 4, 5));

        assertThat(list, everyItem(greaterThan(1)));


    }
    @Test
    public void Asserts(){

        Integer[] ints = new Integer[] { 7, 5, 12, 16 };

        assertThat(ints, arrayWithSize(4));
        assertThat(ints, arrayContaining(7, 5, 12, 16));
    }

    @Test
    public void classes(){
        Todo todo = new Todo(1, "Learn Hamcrest", "Important");

        assertThat(todo, hasProperty("summary"));

        assertThat(todo, hasProperty("summary", equalTo("Learn Hamcrest")));


        Todo todo2 = new Todo(1, "Learn Hamcrest", "Important");
        assertThat(todo, samePropertyValuesAs(todo2));
    }

    @Test
    public void strings(){
        String stringToTest = "";
        assertThat(stringToTest, isEmptyString());
        assertThat(stringToTest, isEmptyOrNullString());
    }
}
