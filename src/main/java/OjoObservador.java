/**
 * Created by romelldominguez on 1/29/17.
 */
public class OjoObservador extends Observador {


    public OjoObservador(DSBStructure dsbStructure) {
        this.dsb = dsbStructure;
        this.dsb.attch(this);
    }

    protected void update() {
        System.out.println("Se ha agregado a la estructura un elemento");
    }
}
