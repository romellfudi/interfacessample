/**
 * Created by romelldominguez on 1/29/17.
 */
public interface DSBContainer {
    DSBIterator iterator();
}
