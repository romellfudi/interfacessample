/**
 * Created by romelldominguez on 1/29/17.
 */
public class Api {

    public static abstract class Callback {
        abstract void throwError(Exception e);

        abstract void Sucessful(Object... objects);

        abstract void Finally();
    }

    public void enque(Callback callback) {
        try {
            enque_(callback);
        } catch (Exception e) {
            callback.throwError(e);
        } finally {
            callback.Finally();
        }
    }

    private void enque_(Callback callback) throws Exception {
        if (false)
            throw new Exception("Problem");
        else
            callback.Sucessful("mensaje ok");
    }
}
