import java.util.ArrayList;
import java.util.List;

/**
 * Created by romelldominguez on 1/29/17.
 */
public class DSBStructure implements DSBContainer{
    public DSBIterator iterator() {
        return new ListIterator();
    }

    private class ListIterator implements DSBIterator{
        int index;
        public boolean hasNext() {
            if (index< list.size())
                return true;
            return false;
        }

        public String next() {
            if (this.hasNext()){
                return list.get(index++);
            }
            return null;
        }
    }

    private ArrayList<String> list = new ArrayList<String>();
    private List<Observador> observadors = new ArrayList<Observador>();

    public void addString(String data) {
        list.add(data);
        notifyNewObservers();
    }

    public void removeString(String data){
        list.remove(data);
        notifyRemoveObservers();
    }

    public void attch(Observador observador) {
        this.observadors.add(observador);
    }

    public void notifyNewObservers() {
        for (Observador observador : this.observadors)
            if (observador instanceof OjoObservador)
                observador.update();
    }

    public void notifyRemoveObservers() {
        for (Observador observador : this.observadors)
            if (observador instanceof SinOjoObservador)
                observador.update();
    }
}
