/**
 * Created by romelldominguez on 1/29/17.
 */
public abstract class Observador {
    protected DSBStructure dsb;

    protected abstract void update();
}
