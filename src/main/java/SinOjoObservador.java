/**
 * Created by romelldominguez on 1/29/17.
 */
public class SinOjoObservador extends Observador {


    public SinOjoObservador(DSBStructure dsbStructure) {
        this.dsb = dsbStructure;
        this.dsb.attch(this);
    }

    protected void update() {
        System.out.println("Se ha eliminado a la estructura un elemento");
    }
}
