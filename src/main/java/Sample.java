/**
 * Created by romelldominguez on 1/29/17.
 */
public class Sample {

    public static void main(String arrgs[]){
        Api api = new Api();
        api.enque(new Api.Callback() {
            void throwError(Exception e) {
                System.out.println(e.getMessage());
            }

            void Sucessful(Object... objects) {
                System.out.println(objects[0]);
            }

            void Finally() {
                System.out.println("Finished");
            }
        });
        Container container = new Container();
        container.setInterfaces(new Plan1());
        container.execute1();
        container.setInterfaces(new Plan2());
        container.execute1();

        DSBStructure dsbStructure = new DSBStructure();
        new OjoObservador(dsbStructure);
        new SinOjoObservador(dsbStructure);
        new SinOjoObservador(dsbStructure);
        new SinOjoObservador(dsbStructure);
        dsbStructure.addString("asdfv");
        dsbStructure.addString("example");
        dsbStructure.addString("abc");
        dsbStructure.removeString("abc");
        for (DSBIterator iterator=dsbStructure.iterator();iterator.hasNext();){
            System.out.println(iterator.next());
        }
    }
}
