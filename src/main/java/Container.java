/**
 * Created by romelldominguez on 1/29/17.
 */
public class Container {
    Interfaces interfaces;


    public void setInterfaces(Interfaces interfaces) {
        this.interfaces = interfaces;
    }

    public void execute1(){
        this.interfaces.method1();
    }

    public void execute2(){
        this.interfaces.method2();
    }
}
