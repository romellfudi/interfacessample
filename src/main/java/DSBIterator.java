/**
 * Created by romelldominguez on 1/29/17.
 */
public interface DSBIterator {
    boolean hasNext();
    String next();
}
